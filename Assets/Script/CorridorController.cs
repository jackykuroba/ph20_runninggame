using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorController : MonoBehaviour
{
    public Transform nextSpawnPoint;
    public BoxCollider spawnNextCollider;

	private bool timerStart = false;
	private float timerCount = 0f;

	private void Awake()
	{
		// 変数の初期化
		timerStart = false; 
		timerCount = 0f;
	}

	private void Update()
	{
		if(UnityChanController.Instance.isDead)
		{
			return;
		}

		transform.Translate(CorridorManager.Instance.moveDir * CorridorManager.Instance.moveSpeed * Time.deltaTime);

		// タイマーのカウント
		if(timerStart)
		{
			timerCount += Time.deltaTime; // タイマーを1フレームの所要時間で増加させる
			if(timerCount >= CorridorManager.Instance.endDuration)
			{
				Destroy(gameObject);
			}
		}

	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			// コライダーを消す
			spawnNextCollider.enabled = false;

			// 新しい廊下を生成する
			CorridorManager.Instance.CreateCorridor(CorridorManager.Instance.lastCorridor.nextSpawnPoint.position);

			// 自己破棄タイマーを起動
			timerStart = true;
			timerCount = 0f;
		}
	}

}
