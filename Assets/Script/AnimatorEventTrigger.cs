using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorEventTrigger : MonoBehaviour
{
	public UnityEvent[] events;

	public void TriggerEvent(int eventIndex)
	{
		if(events.Length == 0)
		{
			return;
		}

		events[eventIndex]?.Invoke();
	}

   
}
