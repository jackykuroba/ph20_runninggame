using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorManager : MonoBehaviour
{
	public static CorridorManager Instance; // ColliderManagerのシングルトンインスタンス

	public Transform initSpawnPoint;
	public GameObject[] corridorPrefabs;

    public CorridorController lastCorridor;

	[Header("廊下の移動用")]
	public float moveSpeed = 10f; // 現在の移動速度
	public float moveSpeedDefault = 10f; // デフォルトの移動速度
	public float moveSpeedIncrement = 5f; // 加速度
	public Vector3 moveDir = new Vector3(0, 0, -1f);
	public float endDuration = 3f;


	private int currentCorridorAmount = 0; // 今まで生成した廊下の数
	private int currentCorridorIndex = 0; // 現在生成した廊下のインデックス


	private void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		}
		else if(Instance != this)
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		// 初期化
		currentCorridorAmount = 0;
		currentCorridorIndex = 0;

		for(int i = 0; i < 5; i++)
		{
			if(lastCorridor == null)
			{
				CreateCorridor(initSpawnPoint.position);
			}
			else
			{
				CreateCorridor(lastCorridor.nextSpawnPoint.position);
			}
		}

		moveSpeed = moveSpeedDefault; // 現在の速度を初期化
	}

	private void Update()
	{
		moveSpeed = moveSpeedDefault +
			(UnityChanController.Instance.level) *
			moveSpeedIncrement; // 速度はレベルによって更新する
	}

	/// <summary>
	/// 廊下を生成する
	/// </summary>
	/// <param name="spawnPoint">生成する座標</param>
	public void CreateCorridor(Vector3 spawnPoint)
	{
		// ランダムで廊下プレハブのインデックスを取得
		//int prefabIndex = Random.Range(0, corridorPrefabs.Length);
		int prefabIndex = 0;
		if(currentCorridorAmount == 0)
		{
			prefabIndex = 0;
		}
		else
		{
			if(currentCorridorIndex != 0)
			{
				prefabIndex = 0;
			}
			else
			{
				prefabIndex = Random.Range(0, corridorPrefabs.Length);
			}
		}

		currentCorridorIndex = prefabIndex;
		currentCorridorAmount++;

		lastCorridor = Instantiate(
					corridorPrefabs[prefabIndex], 
					spawnPoint, 
					Quaternion.identity)
					.GetComponent<CorridorController>();
	}
}
