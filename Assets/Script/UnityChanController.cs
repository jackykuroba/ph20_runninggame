using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UnityChanController : MonoBehaviour
{
	public static UnityChanController Instance; // UnityChanControllerをシングルトン化

	public Transform UnityChanTransform;
	public Transform[] MovePoint;
	public int MovePointIndex = 1;
	//public float moveDistance = 0.5f;
	public float JumpForce = 10f;
	public Rigidbody Rgdbody;

	[Space(10)]
	public Animator anim;
	public TextMeshProUGUI timerText; // タイマーのUI
	public TextMeshProUGUI hiscoreText; // ハイスコアのUI
	private float time; // タイマーの値
	private static float hiScore; // ハイスコアの値
	public int level = 0; // 現在の難易度

	#region プレイヤーフラグ
	public bool isDead = false;
	#endregion

	private void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		} 
		else if(Instance != this)
		{
			Destroy(gameObject);
			return;
		}

		#region プレイヤーフラグ初期化
		isDead = false;
		#endregion

		hiscoreText.text = hiScore.ToString("F2"); // ハイスコアのテキストを更新する
	}

	private void Update()
	{
		UpdateTimer(); // タイマーを更新する関数
	}

	/// <summary>
	/// タイマーを更新する関数
	/// </summary>
	void UpdateTimer()
	{
		if(isDead == false)
		{
			time += Time.deltaTime; // タイマーを更新する
		}

		level = (int)(time / 10); // 難易度を更新する(10秒ごとに)

		timerText.text = time.ToString("F2"); // タイマーテキストを更新（F2：小数点２桁）
	}

	public void OnMoveLeft()
	{
		if(UnityChanTransform == null)
		{
			Debug.Log("Unity ChanのTransformが見つからない");
			return;
		}

		//Vector3 currentPos = UnityChanTransform.position; // 現在の座標を取得
		//currentPos.x -= moveDistance; // 取得した座標のx座標を更新する
		//if(currentPos.x >= -2f) { 
		//	UnityChanTransform.position = currentPos; // 更新した座標を元のTransformに戻す	
		//}

		MovePointIndex--; // 移動ポイントのインデックスを更新
		if(MovePointIndex < 0) // 移動ポイントのインデックスを最小限チェック
		{
			MovePointIndex = 0;
		}
		UnityChanTransform.position = MovePoint[MovePointIndex].position; // インデックスで指定した移動ポイントの座標にUnityChanを移動させる
	}

	public void OnMoveRight()
	{
		if(UnityChanTransform == null)
		{
			Debug.Log("Unity ChanのTransformが見つからない");
			return;
		}

		//Vector3 currentPos = UnityChanTransform.position; // 現在の座標を取得
		//currentPos.x += moveDistance; // 取得した座標のx座標を更新する
		//if(currentPos.x <= 2f)
		//{
		//	UnityChanTransform.position = currentPos; // 更新した座標を元のTransformに戻す
		//}

		MovePointIndex++; // 移動ポイントのインデックスを更新
		if(MovePointIndex >= MovePoint.Length) // 移動ポイントのインデックスを最大限チェック
		{
			MovePointIndex = MovePoint.Length - 1;
		}
		UnityChanTransform.position = MovePoint[MovePointIndex].position; // インデックスで指定した移動ポイントの座標にUnityChanを移動させる
	}

	public void OnJump()
	{
		anim.SetTrigger("Jump");
	}

	public void JumpAction()
	{
		Rgdbody.AddForce(
			Vector3.up * JumpForce, 
			ForceMode.Impulse);
	}


	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Enemy"))
		{
			// 死亡処理
			isDead = true;
			StartCoroutine(RestartStageRoutine(1f));
			if(time > hiScore) // 今のタイマーがハイスコアの値より大きい場合のみハイスコアを更新する
			{
				hiScore = time; // ハイスコアの値を更新する
				hiscoreText.text = hiScore.ToString("F2"); // 更新したハイスコアの値を表示に適用する
			}
		}
	}

	IEnumerator RestartStageRoutine(float delay)
	{
		yield return new WaitForSeconds(delay);

		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}
}
